package ro.zaha.pompi.ui.canale

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CanaleViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            1. Mediul online
            2. Corespondenta
            3. Reclame stradale (pliante/afise)
            4. TV/Radio
            5. Sponsorizari (+un Doamne Ajuta)
        """.trimIndent()
    }
    val text: LiveData<String> = _text
}