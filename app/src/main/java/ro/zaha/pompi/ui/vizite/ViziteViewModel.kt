package ro.zaha.pompi.ui.vizite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ViziteViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            1. Dubai
            2. Egipt
            3. Africa
            4. Ferentari
            5. India
            6. Amsterdam
            7. America
            8. Norvegia
            9. Moscova
            10. North Korea
        """.trimIndent()
    }
    val text: LiveData<String> = _text
}