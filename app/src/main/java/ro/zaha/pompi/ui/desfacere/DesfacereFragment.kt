package ro.zaha.pompi.ui.desfacere

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class DesfacereFragment : Fragment() {

    private lateinit var desfacereViewModel: DesfacereViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        desfacereViewModel =
            ViewModelProviders.of(this).get(DesfacereViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_desfacere, container, false)
        val textView: TextView = root.findViewById(R.id.text_desfacere)
        desfacereViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}