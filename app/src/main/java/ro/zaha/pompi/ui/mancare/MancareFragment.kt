package ro.zaha.pompi.ui.mancare

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class MancareFragment : Fragment() {

    private lateinit var mancareViewModel: MancareViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mancareViewModel =
            ViewModelProviders.of(this).get(MancareViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_mancare, container, false)
        val textView: TextView = root.findViewById(R.id.text_mancare)
        mancareViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}