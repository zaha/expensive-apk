package ro.zaha.pompi.ui.furnizori

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FurnizoriViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "*sunete de greier*"
    }
    val text: LiveData<String> = _text
}