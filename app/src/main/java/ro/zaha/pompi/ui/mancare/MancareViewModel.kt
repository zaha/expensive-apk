package ro.zaha.pompi.ui.mancare

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MancareViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            1. Shaorma
            2. Pizza
            3. Macaroane
            4. Supa
            5. Orez cu lapte
            6. Pilaf
            8. Ochiuri
            9. Catel la gratar
            10. Cartofi taranesti
        """.trimIndent()
    }
    val text: LiveData<String> = _text
}