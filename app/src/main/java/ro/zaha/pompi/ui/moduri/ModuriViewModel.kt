package ro.zaha.pompi.ui.moduri

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ModuriViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            |1. Reclama la TV
            |2. Reclama pe facebook
            |3. E-mailuri
            |4. Pliante
            |5. Rclama in ziar
            |6. Afise
            |7. Reclame pe YouTube
            |8. Reclame la radio
            |9. Sponsorizari la VIP-uri
            |10. Pomelnic la preot
        """.trimMargin()
    }
    val text: LiveData<String> = _text
}