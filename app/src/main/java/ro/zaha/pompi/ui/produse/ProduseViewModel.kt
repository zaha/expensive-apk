package ro.zaha.pompi.ui.produse

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProduseViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            1. Telefon mobil
            2. Laptop
            3. Pasta de dinti
            4. Sampon
            5. Sapun
            6. Lenjerie intima
            7. Apa
            8. Biblia
            9. Hartie
            10. Medicamente
        """.trimIndent()
    }
    val text: LiveData<String> = _text
}