package ro.zaha.pompi.ui.vipuri

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class VipuriFragment : Fragment() {

    private lateinit var vipuriViewModel: VipuriViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vipuriViewModel =
            ViewModelProviders.of(this).get(VipuriViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_vipuri, container, false)
        val textView: TextView = root.findViewById(R.id.text_vipuri)
        vipuriViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}