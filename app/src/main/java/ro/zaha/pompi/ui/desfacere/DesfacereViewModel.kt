package ro.zaha.pompi.ui.desfacere

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DesfacereViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            1. Kamila
            2. Floate de colt
            3. Jolie cala
            4. Rod bun
            5. Bio plant
            6. Vitamix
            7. Prodplant
            8. Biserica ortodoxa romana
            9. Oriflame
            10. Farmasi
        """.trimIndent()
    }
    val text: LiveData<String> = _text
}