package ro.zaha.pompi.ui.vizite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class ViziteFragment : Fragment() {

    private lateinit var viziteViewModel: ViziteViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viziteViewModel =
            ViewModelProviders.of(this).get(ViziteViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_vizite, container, false)
        val textView: TextView = root.findViewById(R.id.text_vizite)
        viziteViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}