package ro.zaha.pompi.ui.concurenti

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class ConcurentiFragment : Fragment() {

    private lateinit var concurentiViewModel: ConcurentiViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        concurentiViewModel =
            ViewModelProviders.of(this).get(ConcurentiViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_concurenti, container, false)
        val textView: TextView = root.findViewById(R.id.text_concurenti)
        concurentiViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}