package ro.zaha.pompi.ui.vipuri

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class VipuriViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            1. Oana Roman
            2. Abi
            3. 5 gang
            4. Pompiliu Teodor
            5. Andrea Esca
            6. Dorian Popa
            7. Dumnezeu
            8. Vulpita
            9. Dana Budeanu
            10. Nicolae Guta
        """.trimIndent()
    }
    val text: LiveData<String> = _text
}