package ro.zaha.pompi.ui.furnizori

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class FurnizoriFragment : Fragment() {

    private lateinit var furnizoriViewModel: FurnizoriViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        furnizoriViewModel =
            ViewModelProviders.of(this).get(FurnizoriViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_furnizori, container, false)
        val textView: TextView = root.findViewById(R.id.text_furnizori)
        furnizoriViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}