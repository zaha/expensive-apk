package ro.zaha.pompi.ui.moduri

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class ModuriFragment : Fragment() {

    private lateinit var moduriViewModel: ModuriViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        moduriViewModel =
            ViewModelProviders.of(this).get(ModuriViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_moduri, container, false)
        val textView: TextView = root.findViewById(R.id.text_moduri)
        moduriViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}