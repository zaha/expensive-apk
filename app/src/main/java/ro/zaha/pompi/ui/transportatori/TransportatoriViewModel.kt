package ro.zaha.pompi.ui.transportatori

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TransportatoriViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            1. Tutu trans
            2. Fan courier
            3. Taxi bravo
            3. Trenu cu nasu
            4. Sameday courier
            5. DHL courier
            6. Urgent cargus
            7. Lelu auto transport
            8. BOBO transport international marfa
            9. Dacia papuc cumparata la 200 lei si tunata
            10. Serban transporturi
        """.trimIndent()
    }
    val text: LiveData<String> = _text
}