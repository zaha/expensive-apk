package ro.zaha.pompi.ui.transportatori

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class TransportatoriFragment : Fragment() {

    private lateinit var transportatoriViewModel: TransportatoriViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        transportatoriViewModel =
            ViewModelProviders.of(this).get(TransportatoriViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_transportatori, container, false)
        val textView: TextView = root.findViewById(R.id.text_transportatori)
        transportatoriViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}