package ro.zaha.pompi.ui.concurenti

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ConcurentiViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            |1. Solaris
            |2. Tisserand
            |3. Savonia
            |4. Vegis
            |5. Essevita
            |6. Oleya
            |7. Lanaform
            |8. Panarom
            |9. doTERRA
            |10. Young Living
        """.trimMargin()
    }
    val text: LiveData<String> = _text
}