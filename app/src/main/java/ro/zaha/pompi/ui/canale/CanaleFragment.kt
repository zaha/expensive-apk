package ro.zaha.pompi.ui.canale

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ro.zaha.pompi.R

class CanaleFragment : Fragment() {

    private lateinit var canaleViewModel: CanaleViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        canaleViewModel =
            ViewModelProviders.of(this).get(CanaleViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_canale, container, false)
        val textView: TextView = root.findViewById(R.id.text_canale)
        canaleViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}