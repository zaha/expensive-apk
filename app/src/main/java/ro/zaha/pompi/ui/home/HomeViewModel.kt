package ro.zaha.pompi.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = """
            |Foaie verde loboda cura apa in beci! :)
            |
            |Aceasta este o aplicatie super mega interesanta.
            |(nu face nimic, apropo)
            |
            |Uleiurile noastre sunt atat de bune incat Oana Roman insasi le foloseste cu litrii!
            |
            |
            |
            |https://www.expensive.ga/
        """.trimMargin()
    }
    val text: LiveData<String> = _text
}